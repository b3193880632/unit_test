const { div_check } = require('../src/util.js');
const { assert } = require('chai');

describe('test_fun_factorials', () => {
  it('test_fun_factorial_5!_is_120', () => {
    assert.isTrue(div_check(5));
    assert.isTrue(div_check(120));
  });

  it('test_fun_factorial_1!_is_1', () => {
    assert.isTrue(div_check(14));
    assert.isTrue(div_check(35));
  });

  it('test_fun_factorial_0!_is_1', () => {
    assert.isFalse(div_check(3));
    assert.isFalse(div_check(11));
  });
  
  it('test_fun_factorial_4!_is_24', () => {
    assert.isFalse(div_check(4));
    assert.isFalse(div_check(24));   
  });

  it('test_fun_factorial_10!_is_3628800', () => {
    assert.isTrue(div_check(10));
    assert.isTrue(div_check(3628800));
  })
});

describe('test_divisibility_by_5_or_7', () => {
    it('test_100_is_divisible_by_5', () => {
        assert.isTrue(div_check(100));
    });

    it('test_49_is_divisible_by_7', () => {
        assert.isTrue(div_check(49));
    });

    it('test_30_is_divisible_by_5', () => {
        assert.isTrue(div_check(30));
    });

    it('test_56_is_divisible_by_7', () => {
        assert.isTrue(div_check(56));
    })
});
